$sourceFile = "$args"

if (!$sourceFile)
{
    Write-Error "No file selected to encode."
    Start-Sleep -Seconds 3
    exit 1
}

$fileName = Split-Path "$sourceFile" -Leaf
$extension = $fileName.Split('.') | Select -Last 1
$fileNameWoExt = $fileName.Substring(0, $fileName.Length - $extension.Length - 1)
$filePath = $sourceFile.Substring(0, $sourceFile.Length - $fileName.Length - 1)

if ($filePath.Chars($filePath.Length-1) -eq ':')
{
    $filePath = $filePath+"\"
}

$outputAppend = "-encoded"

$videoOut = $fileNameWoExt+$outputAppend+".mp4"
$audioOut = $fileNameWoExt+$outputAppend+".m4a"

Write-Host "Original Source: $sourceFile"
Write-Host "Filename: $fileName"
Write-Host "Extension: $extension"
Write-Host "Filename wo Extension: $fileNameWoExt"
Write-Host "FilePath: $filePath"
Write-Host "VideoOut: $videoOut"
Write-Host "AudioOut: $audioOut"

Start-Sleep -Seconds 1

Write-Host "Encoding $fileName to $filePath\$videoOut for Youtube Upload"
&docker run --rm -it --mount type=bind,source="$filePath",target=/config linuxserver/ffmpeg -i /config/"$fileName" -vf yadif,format=yuv420p -force_key_frames "expr:gte(t,n_forced/2)" -c:v libx264 -crf 18 -bf 2 -c:a libfdk_aac -ac 2 -b:a 128k -use_editlist 0 -movflags +faststart "/config/$videoOut"

Write-Host "Extracting and encoding audio from $fileName to $filePath\$audioOut"
# MP3 Encoding
#&docker run --rm -it --mount type=bind,source="$filePath",target=/config linuxserver/ffmpeg -i /config/"$fileName" -vn -map a -c:a mp3 -q:a 1 -ac 2 -ar 48000 "/config/$audioOut.mp3"

# M4A (aac) Encoding
# &docker run --rm -it --mount type=bind,source="$filePath",target=/config linuxserver/ffmpeg -i /config/"$fileName" -vn -map a -c:a libfdk_aac -ac 2 -b:a 128k "/config/$audioOut"

# Extract already encoded AAC from previous step
&docker run --rm -it --mount type=bind,source="$filePath",target=/config linuxserver/ffmpeg -i /config/"$videoOut" -vn -map a -c:a copy "/config/$audioOut"


Write-Host "Completed..."
Start-Sleep -Seconds 3
pause

# SIG # Begin signature block
# MIIFfAYJKoZIhvcNAQcCoIIFbTCCBWkCAQExCzAJBgUrDgMCGgUAMGkGCisGAQQB
# gjcCAQSgWzBZMDQGCisGAQQBgjcCAR4wJgIDAQAABBAfzDtgWUsITrck0sYpfvNR
# AgEAAgEAAgEAAgEAAgEAMCEwCQYFKw4DAhoFAAQUsgr241GFbNcBFgOVKSBc/h70
# YK2gggMSMIIDDjCCAfagAwIBAgIQJ6mF09u8jY1Aamt8sejraTANBgkqhkiG9w0B
# AQUFADAfMR0wGwYDVQQDDBRBbGlhc0lUIENvZGUgU2lnbmluZzAeFw0yMTAzMDkw
# NjM1NDhaFw0yMjAzMDkwNjU1NDhaMB8xHTAbBgNVBAMMFEFsaWFzSVQgQ29kZSBT
# aWduaW5nMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAuApbRlOfEioz
# 9mgLS+2oWems1tWOVXVMtOnZAvk25N+uWi07eYmr0t24omrAMmHv2PiEApa0J4ue
# g4cAaTMtt/oWaPzMoExHP5cKjRJDfMHNbLpWWeiYJJPSzwvssDPVjaeS4hTFgUQD
# LrYctJdgUsZ3AjeNTM8HlVWf78NoZtfxe1n7E4Hd4/knxQEIOUrD0X3cqpbCvuvp
# V/y56wMd2/QiNoJQQTz2pUJ7d8Roszjhcdc+uhVmfZamocyFe6M95lapwjcXM5z+
# ScUlfXWrxpTZarXaPWmM3FnZnr6iU8+Yg11o/r3QYOj6FtvIDJ3uMXx/U7Z3HfZI
# BqpSpu8i/QIDAQABo0YwRDAOBgNVHQ8BAf8EBAMCB4AwEwYDVR0lBAwwCgYIKwYB
# BQUHAwMwHQYDVR0OBBYEFCRQW0At4+i2zZwx+tf1V5K35a4TMA0GCSqGSIb3DQEB
# BQUAA4IBAQBqjPRADCX4A3CcxjELzxW5N5VzKoyZG+RkvUJFDidxBa9Hks4IV56x
# /4WyIxmUNfXnGwFqOWQ2TsOgtiHOV8kvF2gVKy8p3ISTZGBdNCKRf1pmOuPDQx/9
# Vr5gJ80P7pCgAL5/9Zl5Ns6pisX1rykZ1XGHp9eAQJhJ37mSm0dqkqRvF6nNN21e
# N7WonrPwLjXl8pVYR2lTFUJMUAR0g+lU6Z3CIxRBX2ORHfAn44bFOBto4/ogqoXy
# JyWcxJHH2lS2igdS9bJTaEtnSkyncCrdYjI4vvDKgvfdSdUIm4O7GrM1bBiU9gHO
# D6qSk2iMRsTUYmvtop9ovAJSdJM624GWMYIB1DCCAdACAQEwMzAfMR0wGwYDVQQD
# DBRBbGlhc0lUIENvZGUgU2lnbmluZwIQJ6mF09u8jY1Aamt8sejraTAJBgUrDgMC
# GgUAoHgwGAYKKwYBBAGCNwIBDDEKMAigAoAAoQKAADAZBgkqhkiG9w0BCQMxDAYK
# KwYBBAGCNwIBBDAcBgorBgEEAYI3AgELMQ4wDAYKKwYBBAGCNwIBFTAjBgkqhkiG
# 9w0BCQQxFgQUzRMYAlYVvi0nMLSHkRc8bsby2KswDQYJKoZIhvcNAQEBBQAEggEA
# nKxbyFm7x2eU4ceGlx+VAa9zE/5giXSBxuoxDAcVaaPgML/F8Tw5eNsHJI1azeus
# V+UuJ2aKpjtQso7p2rEJpjX1nAfMHMqOicJ2VXSKsDeSoA39pBGW4kQFmCBXiFBd
# mx+Ndlva4vdFqmSMLBt96+rRtzBInYleMNWvu+9A93TEmw34WDHOunjX52m3d3lb
# ig06bDRg2xqKVMFgx7MRtLSgz2aceXfxTgX8lmLP6mPktiKSynroMDWg6lMpV8T6
# T8tWD/as4Ow7rWmc4VdSGKsIBb30VaRipfmhD/hMDyO95bTDRNM0SVhnPuQYbais
# KsPoz7Cqg81CNlcyAOR7/A==
# SIG # End signature block
